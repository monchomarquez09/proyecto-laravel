<?php

use Faker\Generator as Faker;

$factory->define(App\Kardex::class, function (Faker $faker) {
    return [
        //
        'calificacion' => $faker->text(rand(256, 512)),
        'idMateria' => $faker->text(rand(32, 64))
    ];
});
