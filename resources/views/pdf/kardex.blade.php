@extends('layouts.app')
@section('content')

<table class="table table-hover table-striped">
        <thead>
            <tr>
                
              <th>Calificacion</th>
               <th>Materia</th>
            </tr>                            
        </thead>
        <tbody>
            @foreach($kardex as $item)
            <tr>
                <td>{{$item->calificacion}}</td>
                <td>{{ $item->Materias->nombre }}</td>
            </tr>
            @endforeach
        </tbody>
    </table>
@endsection