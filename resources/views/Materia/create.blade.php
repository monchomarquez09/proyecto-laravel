@extends('layouts.app')
@section('content')
<div class="row">
	<section class="content">
		<div class="col-md-8 col-md-offset-2">
			@if (count($errors) > 0)
			<div class="alert alert-danger">
				<strong>Error!</strong> Revise los campos obligatorios.<br><br>
				<ul>
					@foreach ($errors->all() as $error)
					<li>{{ $error }}</li>
					@endforeach
				</ul>
			</div>
			@endif
			@if(Session::has('success'))
			<div class="alert alert-info">
				{{Session::get('success')}}
			</div>
			@endif

			<div class="panel panel-default">
				<div class="panel-heading">
					<h3 class="panel-title">Nueva Materia</h3>
				</div>
				<div class="panel-body">					
					<div class="table-container">
						<form method="POST" action="{{ route('Materia.store') }}"  role="form">
							{{ csrf_field() }}
							<div class="row">
								<div class="col-xs-6 col-sm-6 col-md-6">
									<div class="form-group">
										<input type="text" name="nombre" id="nombre" class="form-control input-sm" placeholder="Nombre de la materia">
									</div>
                                    <div class="form-group">
										<input type="text" name="codigo" id="codigo" class="form-control input-sm" placeholder="codigo de la materia">
									</div>
                                    <div class="form-group">
										<input type="text" name="grado" id="grado" class="form-control input-sm" placeholder="grado de la materia">
									</div>
                                    <div class="form-group">
										<input type="number" name="cal_min" id="cal_min" class="form-control input-sm" placeholder="calificacion minima de la materia">
									</div>
								</div>
								
							</div>

			
	
							<div class="row">
                            <div class="col-xs-6 col-sm-6 col-md-6">
									<input type="submit"  value="Guardar" class="btn btn-success btn-block">
									<a href="{{ route('Materia.index') }}" class="btn btn-info btn-block" >Atrás</a>
								</div>	
                            </div>

                                

							</div>
						</form>
					</div>
				</div>

			</div>
		</div>
	</section>
	@endsection