@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
    @if(Auth::user()->hasRole('admin'))
        <div>Acceso como administrador</div>
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading"><a href="{{ url('/Materia') }}">Materias</a></div>
                <div class="panel-heading"><a href="{{ url('/Kardex') }}">Kardex</a></div>
            </div>
        </div>
    @else
        <div>Acceso usuario</div>
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
            <div class="panel-heading"><a href="{{ url('/Alumno') }}">Alumnos</a></div>
            <div class="panel-heading"><a href="{{ url('/Kardex') }}">Kardex</a></div>

            </div>
        </div>
    @endif
        
    </div>
</div>
@endsection
