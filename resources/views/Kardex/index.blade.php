@extends('layouts.app')
@section('content')
<div class="row">
  <section class="content">
    <div class="col-md-8 col-md-offset-2">
      <div class="panel panel-default">
        <div class="panel-body">
          <div class="pull-left"><h3>Lista Kardex</h3></div>
          <div class="pull-right">
            <div class="btn-group">
              <a href="{{ route('Kardex.create') }}" class="btn btn-info" >Añadir Kardex</a>
            </div>
          </div>
          <div class="table-container">
            <table id="mytable" class="table table-bordred table-striped">
             <thead>
               <th>Calificacion</th>
               <th>Materia</th>
               <th>Editar</th>
               <th>Eliminar</th>
               <th>PDF</th>
             </thead>
             <tbody>
              @if($kardex ->count())  
              @foreach($kardex  as $item)  
              <tr>
              @if($item->calificacion< 8)
              <td style="background-color: red;">{{$item->calificacion}}</td>
              @else
                <td>{{$item->calificacion}}</td>
              @endif
                <td>{{$item->Materias->nombre }}</td>
              
                <td><a class="btn btn-primary btn-xs" href="{{action('KardexController@edit', $item->id)}}" ><span class="glyphicon glyphicon-pencil"></span></a></td>
                <td>
                  <form action="{{action('KardexController@destroy', $item->id)}}" method="post">
                   {{csrf_field()}}
                   <input name="_method" type="hidden" value="DELETE">

                   <button class="btn btn-danger btn-xs" type="submit"><span class="glyphicon glyphicon-trash"></span></button>
                 </td>
                 <td>
                 <p>
                    <a href="{{ route('Kardex') }}" class="btn btn-sm btn-primary">
                        DescargarPDF
                    </a>
                </p>
                 </td>
               </tr>
               @endforeach 
              
               @else
               <tr>
                <td colspan="8">No hay registro !!</td>
              </tr>
              @endif
             
            </tbody>
           
          </table>
        </div>
      </div>
      {{ $kardex->links() }}
    </div>
  </div>
</section>

@endsection