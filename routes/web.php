<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();
Route::middleware('auth')->group(function ( ) {
    Route::get('/home', 'HomeController@index')->name('home');
    Route::resource('Materia', 'MateriaController');
    Route::resource('Alumno', 'AlumnoController');
    Route::resource('Kardex', 'KardexController');

    //Route::get('/', 'KardexController@index')->name('Kardex');
    Route::get('descargar', 'KardexController@pdf')->name('Kardex');
});

