<?php

namespace App\Http\Controllers;
use App\Materia;

use Illuminate\Http\Request;

class MateriaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $materias=Materia::orderBy('id','DESC')->paginate(3);
        return view('Materia.index',compact('materias')); 
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('Materia.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $this->validate($request,[ 'nombre'=>'required', 
        'codigo'=>'required', 'grado'=>'required',
         'cal_min'=>'required']);
        Materia::create($request->all());
        return redirect()->route('Materia.index')->with('success','Registro creado satisfactoriamente');
    
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $materias=Materia::find($id);
        return  view('Materia.show',compact('materias'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $materias=Materia::find($id);
        return view('Materia.edit',compact('materias'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $this->validate($request,[ 'nombre'=>'required', 
        'codigo'=>'required', 'grado'=>'required',
         'cal_min'=>'required']);
 
        Materia::find($id)->update($request->all());
        return redirect()->route('Materia.index')->with('success','Registro actualizado satisfactoriamente');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        Materia::find($id)->delete();
        return redirect()->route('Materia.index')->with('success','Registro eliminado satisfactoriamente');
    
    }
}
