<?php

namespace App\Http\Controllers;
use App\Alumno;

use Illuminate\Http\Request;

class AlumnoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $alumnos=Alumno::orderBy('id','DESC')->paginate(3);
        return view('Alumno.index',compact('alumnos'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('Alumno.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $this->validate($request,[ 'matricula'=>'required', 
        'nombre'=>'required', 'grado'=>'required',
         'carrera'=>'required']);
        Alumno::create($request->all());
        return redirect()->route('Alumno.index')->with('success','Registro creado satisfactoriamente');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $alumnos=Alumno::find($id);
        return  view('Alumno.show',compact('alumnos'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $alumnos=Alumno::find($id);
        return view('Alumno.edit',compact('alumnos'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $this->validate($request,[ 'matricula'=>'required', 
        'nombre'=>'required', 'grado'=>'required',
         'carrera'=>'required']);
 
        Alumno::find($id)->update($request->all());
        return redirect()->route('Alumno.index')->with('success','Registro actualizado satisfactoriamente');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        Alumno::find($id)->delete();
        return redirect()->route('Alumno.index')->with('success','Registro eliminado satisfactoriamente');
    }
}
