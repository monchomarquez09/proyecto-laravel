<?php

namespace App\Http\Controllers;
use App\Kardex;
use App\Materia;
use Illuminate\Http\Request;
use Barryvdh\DomPDF\Facade as PDF;

class KardexController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        //Kardex::with('Materias')->orderBy('id','DESC')->paginate(3);
        $kardex = Kardex::select()->with('Materias')->orderBy('id','DESC')->paginate(3);
        return view('Kardex.index',compact('kardex'));
    }
    public function pdf()
    {        
        /**
         * toma en cuenta que para ver los mismos 
         * datos debemos hacer la misma consulta
        **/
        $kardex = Kardex::all(); 

        $pdf = PDF::loadView('pdf.kardex', compact('kardex'));

        return $pdf->download('listado.pdf');
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $Materias =Materia::orderBy('id','DESC')->paginate(3);
        return view('Kardex.create',compact('Materias'));

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $this->validate($request,[ 'calificacion'=>'required', 
        'idMateria'=>'required']);
        Kardex::create($request->all());
        return redirect()->route('Kardex.index')->with('success','Registro creado satisfactoriamente');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $kardexs=Kardex::find($id);
        return  view('Kardex.show',compact('kardexs'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $Materias =Materia::orderBy('id','DESC')->paginate(3);
        $kardex=Kardex::find($id);
        return view('Kardex.edit',compact('kardex','Materias'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $this->validate($request,[ 'calificacion'=>'required', 
        'idMateria'=>'required']);
 
        Kardex::find($id)->update($request->all());
        return redirect()->route('Kardex.index')->with('success','Registro actualizado satisfactoriamente');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        Kardex::find($id)->delete();
        return redirect()->route('Kardex.index')->with('success','Registro eliminado satisfactoriamente');
    }
}
