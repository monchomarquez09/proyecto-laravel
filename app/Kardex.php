<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Kardex extends Model
{
    //
    protected $table = 'kardex';
    protected $fillable = ['calificacion','idMateria'];
    protected $guarded = ['id'];



    public function Materias()
    {
        //return $this->belongsTo(materia::class,'idMateria');
        //return $this->belongsTo('App\Models\Materia','id','idMateria');
        return $this->belongsTo(Materia::class,'idMateria', 'id');
    }
    
    
}

